# Electron Avalanche

Monte Carlo simulation of electron avalanches in Argon gas

## How To

- Compiling the code:    
`g++ -std=c++11 -o avalanche avalanche.cpp`

- Running the code:    
`./avalanche`

## Output files

- `outputs/avalanche.txt` 
- `outputs/cross-section.txt`
- `outputs/energy.txt` 
- `outputs/frequency.txt` 
- `outputs/trajectory.txt` 
- `outputs/vdf.txt`

## Plots

![avalanche](plots/avalanche.png "Avalanche")
![cross-section](plots/cross-section.png "Cross-sections")
![frequency](plots/frequency.png "Collision frequency")
![drift](plots/drift.png "Drift velocity")
![alpha](plots/alpha.png "First Townsend coeff. (alpha)")
![energy](plots/energy.png "Mean energy")
