/*
 * Monte Carlo simulation of electron avalanches in Argon gas
 * 
 * Copyright 2015 Lucsányi Dávid <lucsanyid@gmail.com>
 * 
 */
 
#define _USE_MATH_DEFINES

#include "electron.h"
#include <stdlib.h>
#include <iostream>
#include <random>
#include <fstream>
#include <sstream>
#include <math.h>
#include <time.h>
#include <queue>
#include <iomanip>
using namespace std;

//*************************
//Switches:
bool Debug = false;
bool TimeMergeForVideo = false;
bool MonteCarlo = true;
//*************************

//Global constants:
const double Emax = 1000; 			        // [eV]
const double q = 1.602176565e-19;	        // [C]
const double m = 9.10938291e-31;	        // [kg]
const double M = 39.948 * 1.6605402e-27;	// [kg]

struct trajectory_struct {
	int id;
	double x;
	double y;
	double z;
	double t;
};
	
struct avalanche_struct {
	double rx;
	double ry;
	double rz;
	double t;
	double vabs; 
	int ID;
};
	
//Homogeneous, permanent electric field (z component)
double ElectricField(double Uanode, double Ucathode, double distance)
{
	//anode and cathode voltages [volt]
	//electrode distance [meter]
	double FieldZ = (Uanode - Ucathode)/distance;  //positive field, direction: cathode <- anode (axis z) 
	return FieldZ; //[V/m]
}
//Uniform random number generator on (0,1)
//-std=gnu++11
double RandGen()
{
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(0, 1);
    return dist(mt);    
}

//Ref.: Phelps and Petrovic, PSST 8, R21 (1999) - Eq. (B.5), (B.6) and (B.7)
//define the elastic scattering, excitation and ionisation cross-sections 
//the latest two processes have an energy threshold 11.5 eV and 15.8 eV. 
double ElasticXS(double E)
{
	double Q = abs( 6/pow((1+E/0.1+(E/0.6)*(E/0.6)),3.3) - (1.1*pow(E,1.4)/(1+pow(E/15,1.2)))/pow(1+pow(E/5.5,2.5) + pow(E/60,4.1),0.5) ) + 0.05/pow(1+E/10,2) + 0.01*pow(E,3)/(1+pow(E/12,6));
	return Q;
	//Q unit: 10^(-20) m^2
}
double ExcitationXS(double E)
{
	double Q;
	if(E<11.5) Q = 0;
	else Q = 0.034*pow(E-11.5,1.1)*(1+pow(E/15,2.8))/(1+pow(E/23,5.5)) + 0.023*(E-11.5)/pow(1+(E/80),1.9);
	return Q;
	//Q unit: 10^(-20) m^2
}
double IonizationXS(double E)  //15.7596 eV
{
	double Q;
	if(E<15.8) Q = 0;
	else Q = 970*(E-15.8)/pow(70+E,2) + 0.06*pow(E-15.8,2)*exp(-E/9);
	return Q;
	//Q unit: 10^(-20) m^2
}
void XSPrintOut()
{
	ofstream XS_file;
	XS_file.open("outputs/cross-section.txt");
	XS_file << fixed << setprecision(6);
	for(int i=0;i<10*Emax;i++)
	{
		double j=(double)i/10;
		XS_file << j << "\t" << ElasticXS(j) << "\t" << ExcitationXS(j) << "\t" << IonizationXS(j) <<endl;
	}
	XS_file.close();
}
double FreqMaximum(double n)
{
	double nu_max = 0;
	for(int i=0;i<100*Emax;i++)
	{
		double j=(double)i/100;  //[eV]
		double vj=sqrt(2*j*q/m); //[m/s]
		double ns = n*vj*1e-20*(ElasticXS(j) + ExcitationXS(j) + IonizationXS(j));
		if(ns > nu_max) nu_max = ns;
	}
	return nu_max;
}
double FreqReal(double n, double E)
{
	double v=sqrt(2*E*q/m); //[m/s]
	double nu = n*v*1e-20*(ElasticXS(E) + ExcitationXS(E) + IonizationXS(E));
	return nu;
}
void FreqPrintOut(double n, double numax)
{
	ofstream Freq_file;
	Freq_file.open("outputs/frequency.txt");
	for(int i=0;i<10*Emax;i++)
	{
		double j=(double)i/10;
		Freq_file << j << "\t" << FreqReal(n,j) << "\t" << numax <<endl;
	}
	Freq_file.close();
}
void TimeMergeOutput(vector<trajectory_struct> traj, double res, double tmax, int N)
{
		double Dt = tmax/res;
		ofstream timetraj_file;
		timetraj_file.open("outputs/trajectory/parameters.txt");
		timetraj_file << res << '\n' << tmax << '\n' << Dt << '\n' << N << endl; 
		timetraj_file.close();	
		for(int i=0; i<=res; i++)
		{
			//~ i*Dt
			string filename = "outputs/trajectory/time." + to_string(i) + ".txt";
			timetraj_file.open(filename);
			for(unsigned j=0; j<traj.size();j++) 
			{
				trajectory_struct out = traj.at(j);
				if(i*Dt<=out.t && out.t<(i+1)*Dt)
				{
					timetraj_file << out.x << '\t' <<out.y << '\t' <<out.z << '\t'<< out.id << endl; 
				}
			}
			timetraj_file.close();	
		}
		cout<<res<<" trajectory file created."<<endl;
}	
void VDF(ofstream& vfile, double frac, double l, vector<double> position, vector<double> velocity)
{
	double vdf_pos = frac * l;  //cm
	double dz = 0.005;	        //cm
	double vr = sqrt(velocity[0]*velocity[0] + velocity[1]*velocity[1]);
	if(vdf_pos-dz<position[2] && vdf_pos+dz>=position[2])
	{
		vfile<<velocity[0]<<"\t"<<velocity[1]<<"\t"<<velocity[2]<<"\t"<<vr<<endl;
	}
}
void distribution(double l, int k)
{  
    ofstream distfile("outputs/energy.txt");
    string line;
    double dz = l/k;
    double alpha;
    double zpos;
    int n=0, dn=0, i;
    	
	vector<double> x, y, z, id, E, time, vz;
	istringstream lin;
	ifstream myfile("outputs/trajectory.txt");
	for (string line; getline(myfile, line); ) 
	{
		lin.clear();
		lin.str(line);
		double a=0, b=0, c=0, d=0, e=0, f=0, g=0;
		if (lin >> a >> b >> c >> d >> e >> f >> g) 
		{
			x.push_back(a);
			y.push_back(b);
			z.push_back(c);
			id.push_back(d);
			E.push_back(e);
			time.push_back(f);
			vz.push_back(g);
		}
		else cout << "WARNING: failed to decode line '" << line << endl;
	}	
	myfile.close();
	
	for(i=1; i<k; i++)
    {
		zpos = i*dz;
		dn = 0;
		double energy_av = 0;
		double vdrift = 0;
		uint j = 0; 
	
		while (j<z.size())
		{		
			if(z.at(j) <= (zpos+dz/2) )
			{
				n++;    //n includes dn too!
				
				if(z.at(j) > (zpos-dz/2))
				{
					energy_av += E.at(j);
					dn++;
					vdrift += vz.at(j);
				}
			}
			j++;
		}

		energy_av /= dn;
		vdrift /= dn;
		alpha = (double)1/(n-dn) * dn/dz;
		if(energy_av>=0 && vdrift>=0) distfile<<zpos<<"\t"<<energy_av<<"\t"<<vdrift<<"\t"<<alpha<<endl;
		
	}
}
	


int main(int argc, char **argv)
{
	clock_t t0,t1,t2;
    t0 = clock();
    t1 = t0;
    double runtime = 0;
    double seconds = 0;

	//Output files:
	ofstream trajectory_file;
	ofstream avalanche_file;
	ofstream vdf_file;
    
	trajectory_file.open("outputs/trajectory.txt");
	avalanche_file.open("outputs/avalanche.txt");
	vdf_file.open("outputs/vdf.txt");
	XSPrintOut();
	
	//###########################		
    //Geometry - 3D cylinder:
    double L = 0.01; 		//[m]
    double refl = 0.05;		//electron reflection from electrodes
    double RelPos = 0.9; 	//relative position for VDF calculation
	
	//Gas parameters:
	//n = N/V = p/kT
	double p = 133.322368;	 			//[Pa] (= 1 Torr)
	double T = 300; 					//[K]
	double n = p/(T * 1.3806488e-23);	//[m^-3]
	
	//Homogenous electric field:
	double voltage = -160.94;
	double Ez = ElectricField(0,voltage,L);
	double E_red = Ez / n * 1e21; 		//[Td]  1 Td = 1e-21 V m^2
	//Ucathode = -(E_red * 1e-21 * n) * L 

	int Naval = 1;					    //Number of avalanches
	//############################
	
	double nu_star = FreqMaximum(n);
	FreqPrintOut(n, nu_star);
	
	double tmax;						//Absolute time in one avalanche
	int N;								//Number of electrons in one avalanche
	int Nall = 0;						//Number of all electrons in the simulation (more than 1 avalanche)
	int Ni;
	vector<trajectory_struct> trajectory;
	
	if(MonteCarlo)	
	{
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		//Multi avalanche loop
		for(Ni=1; Ni<=Naval; Ni++)
		{
			trajectory_struct in;
			avalanche_struct ejected, newejected;
			vector<avalanche_struct> stack;

			ejected.ID = IDGenerator::GetNextID();	
			ejected.rx = 0;
			ejected.ry = 0;
			ejected.rz = 0;
			ejected.t =	 0;
			ejected.vabs = 10000;   // !!!
			stack.push_back(ejected);
			
			in.id = ejected.ID;	
			in.x = ejected.rx;
			in.y = ejected.ry;
			in.z = ejected.rz;
			in.t = ejected.t;
			trajectory.push_back(in);
			
			N = 1;				//Number of all electrons per avalanche
			
			//////////////////////////////////
			cout<<Ni<<". avalanche begin"<<endl;
			int k = 0;
			tmax = 0;				//absolute time
			double dt = 0; 			//electron time
			while(stack.size()) 	//outer loop 
			{
				avalanche_file<<"stack size: "<<stack.size()<<endl;
				avalanche_file<<"N: "<<N<<endl;
				avalanche_file<<"New electrons:"<<endl;
				
				//Initial conditions:
				ejected = stack.front();
				dt = ejected.t;
				avalanche_file<<ejected.ID<<"\t"<<ejected.rx<<"\t"<<ejected.ry<<"\t"<<ejected.rz<<"\t"<<ejected.t<<"\t"<<ejected.vabs<<"\t"<<endl;
				Electron e(ejected.ID, ejected.rx, ejected.ry, ejected.rz, 0, 0, Ez);		//Electric Field!!!
				if(k==0) e.SetVelocity(0,0,10000);          ////
				else e.IsotropeScattering(ejected.vabs);
				stack.erase(stack.begin());
				if(Ni==1 && k==0) trajectory_file<<e.GetPosition()[0]<<"\t"<<e.GetPosition()[1]<<"\t"<<e.GetPosition()[2]<<"\t"<<e.GetID()<<"\t"<<e.GetEnergy()<<"\t"<<dt<<"\t"<<e.GetVelocity()[2];
				else trajectory_file<<endl<<e.GetPosition()[0]<<"\t"<<e.GetPosition()[1]<<"\t"<<e.GetPosition()[2]<<"\t"<<e.GetID()<<"\t"<<e.GetEnergy()<<"\t"<<dt<<"\t"<<e.GetVelocity()[2];
				
				VDF(vdf_file, RelPos, L, e.GetPosition(), e.GetVelocity());
				
				//Internal loop
				while(1)
				{
					//~ cout<<"Internal Loop"<<endl; 
					double eps_new;
					int coll = 0;
					while(!coll)
					{
						//collision time sampling:
						double R1 = RandGen();
						double tau = -1/nu_star * log(R1); 
						
						dt += tau;
						
						//Electric field and velocity vectors scalar product function !!!
						//if electric field changes point by point, then:
						//Efield=GetElectricField(e.GetPosition()); 
						//eps_new += e.CalcDeltaEnergy(Ez,dt);  //Calculating new energy with previous energy
						
						//Equation of Motion
						int eOut = e.MotionEquation(tau,0,0,Ez,L,refl);
						in.id = e.GetID();	
						in.x = e.GetPosition()[0];
						in.y = e.GetPosition()[1];
						in.z = e.GetPosition()[2];
						in.t = dt;
						trajectory.push_back(in);
						trajectory_file<<endl<<e.GetPosition()[0]<<"\t"<<e.GetPosition()[1]<<"\t"<<e.GetPosition()[2]<<"\t"<<e.GetID()<<"\t"<<eps_new<<"\t"<<dt<<"\t"<<e.GetVelocity()[2];
						VDF(vdf_file, RelPos, L, e.GetPosition(), e.GetVelocity());

						if(eOut == 1) 
						{ 
							trajectory_file<<"\t"<<"ELECTRODE";
							goto LABEL; 
						}  // !!!
						if(eOut == 2) 
						{ 
							trajectory_file<<"\t"<<"REFLECTION";
						}
						eps_new = e.GetEnergy();
						double nu_real = FreqReal(n,eps_new);
						double P = nu_real/nu_star;
						double R2 = RandGen();
					
						if(R2<P) coll=1;
					}  
					//COLLISION
					double eps = e.GetEnergy();
					
					double R3 = RandGen();
					double SigmaTot = IonizationXS(eps) + ExcitationXS(eps) + ElasticXS(eps);
			
					double E_ion = 15.8;
					double Echar = 10; //ejected electron spectrum shape parameter, Argon: ~10.0 eV
					double E_exc = 11.5;
					
					//Elastic scattering
					if(R3 < ElasticXS(eps)/SigmaTot)		
					{
						//~ double rel_eps = -2*m*M/pow(m+M,2)*(1-cos(acos(1-2*RandGen())));		//relative energy change
						//~ double new_vabs =  sqrt(2*q*e.GetEnergy()*(rel_eps + 1)/m);				//very small energy loss
						
						//~ e.IsotropeScattering(new_vabs);
						e.IsotropeScattering(-1);           //"very" cold gas approx.
						trajectory_file<<"\t"<<"SCATTERING";
					}
					//Excitation
					else if(R3 >= ElasticXS(eps)/SigmaTot && R3 < (ElasticXS(eps)+ExcitationXS(eps))/SigmaTot)
					{
						//delta_eps = -E_exc
						double new_vabs =  sqrt(2*q*(e.GetEnergy()-E_exc)/m);
						e.IsotropeScattering(new_vabs);
						trajectory_file<<"\t"<<"EXCITATION";
						//excited Ar atom		
					}
					//Ionization
					else if(R3 >= (ElasticXS(eps)+ExcitationXS(eps))/SigmaTot && R3 < 1)
					{
						double eps_ejected = 0;
						double eps_scattered = 0;
						while(eps_ejected <= 0 || eps_scattered <=0)
						{
							eps_ejected = Echar * tan( RandGen()*atan( (eps-E_ion)/(2*Echar) ) );
							//C. B. Opal, W. K. Peterson, E. C. Beaty: Measurements of Secondary Electron Spectra Produced by 
							//Electron Impact Ionization of a Number of Simple Gases
							eps_scattered = eps - E_ion - eps_ejected;
						}
						double vabs_scattered = sqrt(2*q*eps_scattered/m);	
						double vabs_ejected = sqrt(2*q*eps_ejected/m);			
						
						e.IsotropeScattering(vabs_scattered);
						
						vector<double> r_ion = e.GetPosition();
										
						//electron stack fill
						newejected.ID = IDGenerator::GetNextID();
						newejected.rx = r_ion[0];
						newejected.ry = r_ion[1];
						newejected.rz = r_ion[2];
						newejected.t = dt;
						newejected.vabs = vabs_ejected;
						avalanche_file<<newejected.ID<<"\t"<<newejected.rx<<"\t"<<newejected.ry<<"\t"<<newejected.rz<<"\t"<<newejected.t <<"\t"<<newejected.vabs<<"\t"<<endl;
						stack.push_back(newejected);
						N++;
						trajectory_file<<"\t"<<"IONIZATION";
					}
					
				}
				LABEL:  //goto label: the code jumps here, whenever an electron is captured in the electrode! 
				if(dt > tmax) tmax = dt;
				k++;
			
			}
			//////////While loop end//////////
			avalanche_file.close();
			vdf_file.close();
			Nall += N;
			cout<<"All number of electron (N): "<<N<<endl;
			cout<<"Avalanche total time (t): "<<tmax*1e9<<" ns"<<endl;
			t2 = clock();
			runtime = t2 - t1;
			t1 = t2;
			seconds = runtime / CLOCKS_PER_SEC;
			cout<<"Calculation time: "<<seconds<<" sec"<<endl;
			cout<<"Avalanche ended"<<endl<<endl;
		}
		//%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
		trajectory_file.close();
	}
	
	cout<<Ni-1<<" avalanche simulated"<<endl;
	cout<<"Voltage: "<<voltage<<" V"<<endl;
	cout<<"Length: "<<L*100<<" cm"<<endl;
	cout<<"Pressure: "<<p<<" Pa"<<endl;
	cout<<"E/n: "<<E_red<<" Td"<<endl;
	cout<<"Temparature: "<<T<<" K"<<endl;
	cout<<"Density: "<<n*1e-6<<" cm^-3"<<endl;
	cout<<"Reflectivity: "<<refl<<endl;
	cout<<"All electron in the simulation (Nall): "<<Nall<<endl;
		
	//##############################
	//Data Analysis	
	int Zresolution = 200;
	distribution(L, Zresolution);

	double resolution = 200;
	if(TimeMergeForVideo) TimeMergeOutput(trajectory, resolution, tmax, N);
	//##############################
	
	t2 = clock();
    runtime = t2 - t0;
    seconds = runtime / CLOCKS_PER_SEC;
	cout<<"\nTotal running time: "<<seconds<<" sec"<<endl;

	return 0;	
}
