/*
 * Monte Carlo simulation of electron avalanches in Argon gas
 * 
 * Copyright 2015 Lucsányi Dávid <lucsanyid@gmail.com>
 * 
 */

#ifndef electron_h
#define electron_h

#include <iostream>
#include <vector>
#include <cmath>
#include <random>
using namespace std;

//ID Generator for e-ion pairs:
class IDGenerator
{
private:
    static int s_nNextID;
 
public:
     static int GetNextID() { return s_nNextID++; }
};
// We'll start generating IDs at 0
int IDGenerator::s_nNextID = 0;


class Electron
{
private:
    int e_ID;
    double e_ax, e_ay, e_az;
    double e_vx, e_vy, e_vz;
    double e_rx, e_ry, e_rz;	
    double e_v;	
    double e_energy;
    
    //Constants:
	const double q = 1.602176565e-19;
	const double m = 9.10938291e-31;
	
public:    
	//Default constructor
	Electron(int id=-1, double rx=0, double ry=0, double rz=0, double Ex=0, double Ey=0, double Ez=0)
    {
        SetPosition(rx, ry, rz);
		SetVelocity(0, 0, 0);
		SetAcceleration(q*Ex/m, q*Ey/m, q*Ez/m);
		//~ SetAcceleration(0, 0, 0);
		e_ID = id;
    } //different constructors: (E,v(x,y,z))
        
    //Destructor
    ~Electron(){}
    //Random Generator
    double eRandGen();
    //Getters
	vector<double> GetPosition();
	vector<double> GetVelocity();
	vector<double> GetAcceleration();
	double GetEnergy();
	int GetID();
	//Setters
    void SetPosition(double rx, double ry, double rz);
    void SetVelocity(double vx, double vy, double vz);
    void SetAcceleration(double ax, double ay, double az);
    void SetEnergy(double vabs);
    
    //~ double CalcDeltaEnergy(double EfieldZ, double dt);
	int MotionEquation(double dt, double Ex, double Ey, double Ez, double l, double rc);
	void IsotropeScattering(double v_abs = -1);

	
};  //here semicolon after class def!	

//Uniform random number generator on (0,1)
//-std=gnu++11
double Electron::eRandGen()
{
    random_device rd;
    mt19937 mt(rd());
    uniform_real_distribution<double> dist(0, 1);
    return dist(mt);    
}	
vector<double> Electron::GetPosition()
{
	size_t vectorsize = 3;
	vector<double> r_vector(vectorsize);
	r_vector[0] = e_rx;
	r_vector[1] = e_ry;
	r_vector[2] = e_rz;
	return r_vector; 
}	
vector<double> Electron::GetVelocity()
{
	size_t vectorsize = 4;
	vector<double> v_vector(vectorsize);
	v_vector[0] = e_vx;
	v_vector[1] = e_vy;
	v_vector[2] = e_vz;
	v_vector[3] = e_v;
	return v_vector; 
}
vector<double> Electron::GetAcceleration()
{
	size_t vectorsize = 3;
	vector<double> a_vector(vectorsize);
	a_vector[0] = e_ax;
	a_vector[1] = e_ay;
	a_vector[2] = e_az;
	return a_vector; 
}
double Electron::GetEnergy()
{
	return e_energy; 
}
int Electron::GetID() { return e_ID; }
void Electron::SetPosition(double rx, double ry, double rz)
{
	e_rx = rx; 
	e_ry = ry; 
	e_rz = rz; 
}
void Electron::SetVelocity(double vx, double vy, double vz)
{
	e_vx = vx; 
	e_vy = vy; 
	e_vz = vz;
	e_v = sqrt(vx*vx + vy*vy + vz*vz);
	SetEnergy(e_v);
}
void Electron::SetAcceleration(double ax, double ay, double az)
{
	e_ax = ax; 
	e_ay = ay; 
	e_az = az; 
}
void Electron::SetEnergy(double vabs)
{
	e_energy = m*vabs*vabs/(2*q); //[eV]				
}
//Integrate of the Equation of motion  (Velocity Verlet)
int Electron::MotionEquation(double dt, double Ex, double Ey, double Ez, double l, double rc)
{
	int eOut = 0;
	//t=t0+dt   VELOCITY VERLET 
    //this algorithm assumes that acceleration a(t+dt) only depends on position x(t+dt), and does not depend on velocity v(t+dt)
	vector<double> r0 = GetPosition();
	if(r0[2] < 0 || r0[2] > l) return eOut = 1; 
	vector<double> v0 = GetVelocity();
	vector<double> a0 = GetAcceleration();
	//r(t0+dt) vector:
	vector<double> new_r(3);
	new_r[0] = a0[0]*(dt*dt)/2 + v0[0]*dt + r0[0];
	new_r[1] = a0[1]*(dt*dt)/2 + v0[1]*dt + r0[1];
	new_r[2] = a0[2]*(dt*dt)/2 + v0[2]*dt + r0[2];
	
	//Boundary conditions and reflection:
    if(new_r[2] <= 0 || new_r[2] >= l) 
    {
		double r_el;
		if(abs(l-new_r[2]) < abs(0-new_r[2])) r_el = l;	
		else r_el = 0;
		double t2el = (-v0[2] + sqrt(v0[2]*v0[2] + 2*a0[2]*(r_el - r0[2]))) / a0[2]; 	//time to electrode
		
		if (eRandGen() < rc)	//reflection from electrode
		{
			eOut = 2;
			r0[2] = r0[2] + v0[2]*t2el + a0[2]*t2el*t2el/2;  	//electrode position
			//a0'[2] = a0[2]
			v0[2] = -(a0[2]*t2el + v0[2]);		//new velocity in moment of reflection
			new_r[2] = r0[2] + v0[2]*(dt-t2el) + a0[2]*(dt-t2el)*(dt-t2el)/2;	//new position after reflection
			if(new_r[2] > l) new_r[2] = l; 		//eh... (nothing is perfect)
		}
		else 	//absorption in electrode
		{
			eOut = 1;
			new_r[2] = r_el;
			SetVelocity( a0[0]*t2el + v0[0], a0[1]*t2el + v0[1], a0[2]*t2el + v0[2] );
		}
	}
	SetPosition(new_r[0], new_r[1], new_r[2]);
	if(eOut != 1)
	{
		//deriving a(t0+dt) vector from r(t0+dt) vector:
		vector<double> new_a(3);
		new_a[0] = q*Ex/m;			//new function: double GetEfieldX(vector<double> r) >>> e_ax = q*GetEfieldX(GetPosition())/m;
		new_a[1] = q*Ey/m;
		new_a[2] = q*Ez/m;
		SetAcceleration(new_a[0], new_a[1], new_a[2]);
		//v(t0+dt) vector:
		SetVelocity( (a0[0]+new_a[0])*dt/2 + v0[0], (a0[1]+new_a[1])*dt/2 + v0[1], (a0[2]+new_a[2])*dt/2 + v0[2] );
	}
	return eOut;
	
	//Numeric integration if Ex is not independent from x:
	//http://www.fmt.bme.hu/fmt/oktatas/feltoltesek/bmeeoftmkt2/fejezet_06.pdf
}
void Electron::IsotropeScattering(double v_abs)
{
	double eta = 2*M_PI*eRandGen();			//Eta  [0,2pi]
	double kszi = acos(1-2*eRandGen());		//Kszi [0,pi]
	vector<double> v_new(3);
	
	if (v_abs == -1) v_abs = GetVelocity()[3];
		
	v_new[0] = v_abs*cos(eta)*sin(kszi);
	v_new[1] = v_abs*sin(eta)*sin(kszi);
	v_new[2] = v_abs*cos(kszi);
	
	SetVelocity(v_new[0],v_new[1],v_new[2]);
	
	//http://fizipedia.bme.hu/index.php/T%C3%A9r_%C3%A9s_id%C5%91
}
#endif
