reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'alpha.png'
set yrange [0:1]
set ylabel "{/Symbol a} [1/cm]"
set xlabel "z [cm]"
set title "First Townsend ionisation coefficient (L=1 cm, p=1 Torr, T=300 K)"
plot 'energy300.txt' using ($1*100):($4/100) title "E/n = 300 Td" smooth csplines w l, 'energy500.txt' using ($1*100):($4/100) title "E/n = 500 Td" smooth csplines w l, 'energy700.txt' using ($1*100):($4/100) title "E/n = 700 Td" smooth csplines w l

