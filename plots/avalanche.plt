reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'avalanche.png'
set xrange [-0.6:0.6]
set ylabel "z [cm]"
set xlabel "x [cm]"
set title "Electron avalanche (L=1 cm, p=1 Torr, T=300 K, E/n=700 Td)"
plot 'trajectory.txt' using ($1*100):($3*100) title "" w p
