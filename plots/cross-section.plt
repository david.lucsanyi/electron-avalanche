reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'cross-section.png'
set xrange [0.1:400]
set logscale x
set logscale y
set ylabel "{/Symbol s} [10^{-16}cm^2]"
set xlabel "{/Symbol e} [eV]"
set title "Electron-Argon cross-sections"
plot 'cross-section.txt' using 1:($2) title "elastic scattering" w l lt 1 lc 1, 'cross-section.txt' using 1:($3) title "excitation" w l lt 1 lc 6, 'cross-section.txt' using 1:($4) title "ionisation" w l lt 1 lc 4
