reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'drift.png'
set ylabel "v_{drift} [10^6m/s]"
set xlabel "z [cm]"
set title "Electron drift velocity (L=1 cm, p=1 Torr, T=300 K)"
plot 'energy300.txt' using ($1*100):($3*1e-6) title "E/n = 300 Td" smooth csplines w l, 'energy500.txt' using ($1*100):($3*1e-6) title "E/n = 500 Td" smooth csplines w l, 'energy700.txt' using ($1*100):($3*1e-6) title "E/n = 700 Td" smooth csplines w l

