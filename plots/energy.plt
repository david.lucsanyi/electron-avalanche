reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'energy.png'
set yrange [0:18]
set ylabel "{/Symbol e} [eV]"
set xlabel "z [cm]"
set title "Mean electron energy (L=1 cm, p=1 Torr, T=300 K)"
plot 'energy300.txt' using ($1*100):2 title "E/n = 300 Td" smooth csplines w l, 'energy500.txt' using ($1*100):2 title "E/n = 500 Td" smooth csplines w l, 'energy700.txt' using ($1*100):2 title "E/n = 700 Td" smooth csplines w l

