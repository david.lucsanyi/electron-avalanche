reset
set terminal png enhanced font "Helvetica" 20 size 900,600
set output 'frequency.png'
set xrange [0.1:400]
set yrange [0:14]
set logscale x
set xlabel "{/Symbol e} [eV]"
set ylabel "{/Symbol n} [10^9 1/s]"
set title "Collision frequency"
plot 'frequency.txt' using 1:($2*1e-9) title "actual" w l lc 3, 'frequency.txt' using 1:($3*1e-9) title "maximum" w l lc 5
